# Changelog

### 1.0.2 - 2018-06-01
- Attachments via mail will be sent only if the new config directive ```'email_report'/'script_debug'``` is ```TRUE```
- Original source files before and after a sync will be attached to debug the behaviour of the script when changes occur
- Small fix: if an event has changed in content and in status will be no more duplicated in report as UPDATED and RESUMED

### 1.0.1 - 2018-05-28
- The email report has been made clearer and more readable

### 1.0.0 - 2018-05-25
- Initial release