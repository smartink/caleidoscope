#!/usr/bin/env bash
#
# Config file for gcal-sync-*.sh scripts
#

# PHP Interpreter path - leave empty to use php in path
PHP=""

# WPCLI credentials for gcal-sync-wps
WPCLI_SSH_USER=username
WPCLI_SSH_HOST=www.example.net
WPCLI_WP_PATH=/path/to/www.example.net
WPCLI_WP_USER=info@example.net