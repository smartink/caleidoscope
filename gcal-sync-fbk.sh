#!/bin/bash
#
# gcalsyncer.php launcher (facebook events)
#
#

ME=$(readlink -f "$0")
ME_DIR=$(dirname "$ME")

### CONFIG ###########################################################
# Config file
if [ ! -f ${ME_DIR}/config/config.sh ]; then
    echo "FATAL! config/config.sh not found, you need to create it starting from the example" >&2
    exit 1
fi
source ${ME_DIR}/config/config.sh

if [ "$PHP" = "" ]; then
    PHP="php"
fi

GCAL_SYNC_SCRIPT=${ME_DIR}/gcalsyncer.php
LOGFILE=${ME_DIR}/var/gcal-sync-fbk.log
export TZ=Europe/Rome
######################################################################

# Handles the exit
# If the passed errno is not 0, send the log to output
# $1 : errno
#
exit_script () {
   
    if [ $1 -ne 0 ]; then
	    cat ${LOGFILE}
    fi

    exit $1
}


# Reset log-file
echo -n "" > ${LOGFILE}

# Google Calendar Syncer execution
echo "$(date) INFO launching gcalsyncer" >> ${LOGFILE}
${PHP} ${GCAL_SYNC_SCRIPT} -A dosync:facebook -M >> ${LOGFILE} 2>> ${LOGFILE}
gcserrno=$?
if [ ${gcserrno} -ne 0 ]; then
    echo "$(date) ERROR executing gcalsyncer, errno=${gcserrno}" >> ${LOGFILE}
    exit_script 3
else
    echo "$(date) DONE executed gcalsyncer" >> ${LOGFILE}
fi

exit_script 0
