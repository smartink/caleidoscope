#!/bin/bash
#
# Downloads events and venues from the configured Wordpress Event Calendar source via wp-cli command
# and launch gcalsyncer.php from wordpress
#
#

ME=$(readlink -f "$0")
ME_DIR=$(dirname "$ME")

### CONFIG ###########################################################
# Config file
if [ ! -f ${ME_DIR}/config/config.sh ]; then
    echo "FATAL! config/config.sh not found, you need to create it starting from the example" >&2
    exit 1
fi
source ${ME_DIR}/config/config.sh

if [ "$PHP" = "" ]; then
    PHP="php"
fi
if [ "$WP" = "" ]; then
    WP="wp"
fi

GCAL_SYNC_SCRIPT=${ME_DIR}/gcalsyncer.php
GCAL_SYNC_DATADIR=${ME_DIR}/data/
LOGFILE=${ME_DIR}/var/gcal-sync-wps.log
export TZ=Europe/Rome
######################################################################

# Handles the exit
# If the passed errno is not 0, send the log to output
# $1 : errno
#
exit_script () {
   
    if [ $1 -ne 0 ]; then
	cat ${LOGFILE}
    fi

    exit $1
}


# Reset log-file
echo -n "" > ${LOGFILE}

# Save backup of xml files if existing before overwrite them
if [ -f ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-venues.xml ]; then
    cp ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-venues.xml ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-venues-old.xml 2>/dev/null >/dev/null
fi
if [ -f${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-events.xml ];then
    cp ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-events.xml ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-events-old.xml 2>/dev/null >/dev/null
fi

# Download venues
echo "$(date) INFO downloading wp-venues" >> ${LOGFILE}
${WP} export --stdout --ssh=${WPCLI_SSH_USER}@${WPCLI_SSH_HOST}:${WPCLI_WP_PATH} --user=${WPCLI_WP_USER} --post_type=tribe_venue --max_file_size=-1 > ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-venues.xml.dwn 2>> ${LOGFILE}
wperrno=$?
if [ ${wperrno} -ne 0 ]; then
    echo "$(date) ERROR downloading wp-venues, wp errno=${wperrno}" >> ${LOGFILE}
    exit_script 1
else
    echo "$(date) INFO downloaded wp-venues" >> ${LOGFILE}
fi

# Download events
echo "$(date) INFO downloading wp-events" >> ${LOGFILE}
${WP} export --stdout --ssh=${WPCLI_SSH_USER}@${WPCLI_SSH_HOST}:${WPCLI_WP_PATH} --user=${WPCLI_WP_USER} --post_type=tribe_events --max_file_size=-1 > ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-events.xml.dwn && 2>> ${LOGFILE}
wperrno=$?
if [ ${wperrno} -ne 0 ]; then
    echo "$(date) ERROR downloading wp-events, wp errno=${wperrno}" >> ${LOGFILE}
    exit_script 2
else
    echo "$(date) INFO downloaded wp-events" >> ${LOGFILE}
fi

# Downloaded files can be renamed
mv -f ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-venues.xml.dwn ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-venues.xml 2>> ${LOGFILE}
mv -f ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-events.xml.dwn ${GCAL_SYNC_DATADIR}/stewfun-wordpress-wp-events.xml 2>> ${LOGFILE}

# Google Calendar Syncer execution
echo "$(date) INFO launching gcalsyncer" >> ${LOGFILE}
${PHP} ${GCAL_SYNC_SCRIPT} -A dosync:wordpress -M >> ${LOGFILE} 2>> ${LOGFILE}
gcserrno=$?
if [ ${gcserrno} -ne 0 ]; then
    echo "$(date) ERROR executing gcalsyncer, errno=${gcserrno}" >> ${LOGFILE}
    exit_script 3
else
    echo "$(date) DONE executed gcalsyncer" >> ${LOGFILE}
fi

exit_script 0
