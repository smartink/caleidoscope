<?php
/**
 * Parsing of events and venues XML exports from wordpress
 */

namespace stewfun\wordpress;

use Exception;
use stewfun\common\Calendar as CommonCalendar;

class EventXML
{

    private $wpEventsFile = null;
    private $wpVenuesFile = null;

    private $sourceHasChanged = false;

    private $wpEventsXML = null;
    private $wpVenuesXML = null;

    private $fileSources = [];

    private $optParams = [];


    /**
     * Constructor.
     *
     * @param string $wpEventsFile Path to xml file with events
     * @param string $wpVenuesFile Path to xml file with events' venues
     * @param array  $optParams    Optional config parameters
     *
     * @throws Exception
     */
    public function __construct($wpEventsFile, $wpVenuesFile, $optParams = [])
    {

        $this->wpEventsFile = $wpEventsFile;
        $this->wpVenuesFile = $wpVenuesFile;

        $this->wpEventsXML = $this->parseXML($wpEventsFile);
        $this->wpVenuesXML = $this->parseXML($wpVenuesFile);

        $this->setFileSources();

        $this->optParams = array_replace_recursive([
            // If TRUE, the timezone provided in the XML is ignored and timestamps
            // are considered belonging the script timezone
            // Useful when the timezone in the XML is wrong (default FALSE)
            'ignore_provided_timezone' => false
        ], $optParams);
    }

    /**
     * Returns the used file sources
     *
     * @return array
     */
    public function getFileSources()
    {
        return $this->fileSources;
    }

    /**
     * Returns TRUE if the source has changed since last check
     *
     * @return bool
     */
    public function getSourceHasChanged()
    {
        return $this->checkSourcesHaveChanged();
    }

    /**
     * Returns an associative array of events
     *
     * @return array
     */
    public function fetchAllAssoc()
    {

        //
        // VENUES
        //
        $venuesAllAssoc = [];
        foreach ($this->wpVenuesXML->channel->item as $item) {

            $venueAssoc = [
                'id'             => (string)$item->children('wp', true)->post_id,
                'slug'           => (string)$item->children('wp', true)->post_name,
                'title'          => (string)$item->title,
                'link'           => (string)$item->link,
                'full_address'   => null,
                'address'        => null,
                'city'           => null,
                'country'        => null,
                'province'       => null,
                'state'          => null,
                'zip'            => null,
                'state_province' => null,
                'phone'          => null,
                'url'            => null,
            ];

            foreach ($item->children('wp', true)->postmeta as $postmeta) {

                $meta_key   = (string)$postmeta->children('wp', true)->meta_key;
                $meta_value = (string)$postmeta->children('wp', true)->meta_value;

                switch ($meta_key) {
                    case '_VenueAddress' :
                        $assocKey = 'address';
                        break;
                    case '_VenueCity'   :
                        $assocKey = 'city';
                        break;
                    case '_VenueCountry' :
                        $assocKey = 'country';
                        break;
                    case '_VenueProvince'   :
                        $assocKey = 'province';
                        break;
                    case '_VenueState' :
                        $assocKey = 'state';
                        break;
                    case '_VenueZip'   :
                        $assocKey = 'zip';
                        break;
                    case '_VenuePhone'   :
                        $assocKey = 'phone';
                        break;
                    case '_VenueURL'   :
                        $assocKey = 'url';
                        break;
                    case '_VenueStateProvince'   :
                        $assocKey = 'state_province';
                        break;
                    default:
                        continue 2;
                }

                $venueAssoc[$assocKey] = $meta_value;
            }

            $venueAssoc['full_address'] = $this->mergeAddress(
                $venueAssoc['address'], $venueAssoc['city'], $venueAssoc['country'], $venueAssoc['province'],
                $venueAssoc['state'], $venueAssoc['zip'], $venueAssoc['state_province']);

            $venuesAllAssoc[$venueAssoc['id']] = $venueAssoc;
        }

        //
        // EVENTS
        //
        $min_start_ts   = null;
        $max_end_ts     = null;
        $statuses       = [];
        $total_events   = 0;
        $eventsAllAssoc = [];
        foreach ($this->wpEventsXML->channel->item as $item) {

            $eventAssoc = [
                'id'            => (string)$item->children('wp', true)->post_id,
                'iCalUID'       => null,
                'venue_id'      => null,
                'slug'          => (string)$item->children('wp', true)->post_name,
                'status'        => (string)$item->children('wp', true)->status,
                'create_date'   => (string)$item->children('wp', true)->post_date,
                'pub_date'      => (string)$item->pubDate,
                'category_slug' => (string)$item->category['nicename'],
                'category_name' => (string)$item->category,
                'start_ts'      => null,
                'end_ts'        => null,
                'timezone'      => null,
                'title'         => (string)$item->title,
                'sub_title'     => null,
                'currency'      => null,
                'cost'          => null,
                'url'           => null,
                'link'          => (string)$item->link,
                'excerpt'       => (string)$item->children('excerpt', true)->encoded,
                'content'       => (string)$item->children('content', true)->encoded,
                'description'   => (string)$item->description,
                '_venue_'       => null
            ];

            foreach ($item->children('wp', true)->postmeta as $postmeta) {

                $meta_key   = (string)$postmeta->children('wp', true)->meta_key;
                $meta_value = (string)$postmeta->children('wp', true)->meta_value;

                switch ($meta_key) {
                    case '_EventStartDate' :
                        $assocKey = 'start_ts';
                        break;
                    case '_EventEndDate'   :
                        $assocKey = 'end_ts';
                        break;
                    case '_EventCurrencySymbol' :
                        $assocKey = 'currency';
                        break;
                    case '_EventCost'   :
                        $assocKey = 'cost';
                        break;
                    case '_EventURL'   :
                        $assocKey = 'url';
                        break;
                    case '_EventTimezone'   :
                        $assocKey = 'timezone';
                        break;
                    case '_EventVenueID'   :
                        $assocKey = 'venue_id';
                        break;
                    case 'wps_subtitle'   :
                        $assocKey = 'sub_title';
                        break;
                    default:
                        continue 2;
                }

                $eventAssoc[$assocKey] = $meta_value;
            }

            // Saving related venue
            if (isset($venuesAllAssoc[$eventAssoc['venue_id']])) {
                $eventAssoc['_venue_'] = $venuesAllAssoc[$eventAssoc['venue_id']];
            }

            // Datetimes normalization
            $timezoneToUse          = ($this->optParams['ignore_provided_timezone'] ? '' : $eventAssoc['timezone']);
            $eventAssoc['start_ts'] = CommonCalendar::timeToIso8601($eventAssoc['start_ts'], $timezoneToUse);
            $eventAssoc['end_ts']   = CommonCalendar::timeToIso8601($eventAssoc['end_ts'], $timezoneToUse);
            $eventAssoc['timezone'] = CommonCalendar::sanitizeTimezoneIdentifier($eventAssoc['timezone']);

            // Skip malformed events without start/end dates
            if (is_null($eventAssoc['start_ts']) || is_null($eventAssoc['end_ts'])) {
                continue;
            }

            // Calculate ical uid
            $eventAssoc['iCalUID'] = $eventAssoc['id'] . 'ts' . date('YmdHis', strtotime($eventAssoc['create_date'])) . '@dmpwp';

            // Last formatting
            $eventAssoc = $this->eventFormat($eventAssoc);

            // Saving event
            $eventsAllAssoc[$eventAssoc['iCalUID']] = $eventAssoc;

            // Metadata
            if (is_null($min_start_ts) ||
                (!empty($eventAssoc['start_ts']) && $eventAssoc['start_ts'] < $min_start_ts)) {
                $min_start_ts = $eventAssoc['start_ts'];
            }
            if (is_null($max_end_ts) ||
                (!empty($eventAssoc['end_ts']) && $eventAssoc['end_ts'] > $max_end_ts)) {
                $max_end_ts = $eventAssoc['end_ts'];
            }
            if (!isset($statuses[$eventAssoc['status']])) {
                $statuses[$eventAssoc['status']] = 1;
            } else {
                $statuses[$eventAssoc['status']]++;
            }
            $total_events++;
        }


        //
        // Merge with metadata
        //
        $eventDataSet = [
            'meta'   => [
                'min_start_ts' => $min_start_ts,
                'max_end_ts'   => $max_end_ts,
                'statuses'     => $statuses,
                'total_events' => $total_events,

            ],
            'events' => $eventsAllAssoc
        ];

        return $eventDataSet;
    }


    /**
     * Build filesources list and write it into $this->fileSources
     *
     * @return void
     */
    private function setFileSources()
    {
        $fileSources = [$this->wpVenuesFile, $this->wpEventsFileFile];

        // '-old' renamed versions
        $oldFileSources = [];
        foreach ($fileSources as $f) {
            $fPathInfo        = pathinfo($f);
            $oldFileSources[] = $fPathInfo['dirname'] . '/' . $fPathInfo['filename'] . '-old.' . $fPathInfo['extension'];
        }

        $this->fileSources = array_merge($fileSources, $oldFileSources);
    }

    /**
     * Checks, set and returns if datasources have changed since last check or not
     *
     * @return bool
     */
    private function checkSourcesHaveChanged()
    {
        // File in which hashes are stored
        $srcHashesFile = dirname($this->wpEventsFile) . "/stewfun-wordpress-wp-hashes.txt";

        // Calculates actual hashes
        $wpEventsHash = $this->getDataSourceHash($this->wpEventsFile);
        $wpVenuesHash = $this->getDataSourceHash($this->wpVenuesFile);

        // Hashes file not found, we mark the sources changed since last time
        if (!is_file($srcHashesFile)) {
            $this->sourceHasChanged = true;
        } // Hashes file found, we read it
        else {
            $lastHashes = explode("|", trim(file_get_contents($srcHashesFile)));

            if (is_null($wpEventsHash) || is_null($wpVenuesHash) || !is_array($lastHashes) ||
                !isset($lastHashes[0]) || trim($lastHashes[0]) == '' ||
                !isset($lastHashes[1]) || trim($lastHashes[1]) == '' ||
                $lastHashes[0] != $wpEventsHash || $lastHashes[1] != $wpVenuesHash) {
                $this->sourceHasChanged = true;
            } else {
                $this->sourceHasChanged = false;
            }
        }

        // If source is changed since last time we write/update the hashes file
        if ($this->sourceHasChanged == true) {
            file_put_contents($srcHashesFile, implode('|', [$wpEventsHash, $wpVenuesHash]));
        }

        return $this->sourceHasChanged;
    }

    /**
     * Returns the hash of the file representing the data-source, NULL if not present
     *
     * @param string $dataSourceFile Path to the source saved locally as a file
     *
     * @return null|string
     */
    private function getDataSourceHash($dataSourceFile)
    {
        if (!file_exists($dataSourceFile) || !is_file($dataSourceFile) || !is_readable($dataSourceFile)) {
            return NULL;
        }

        $dataSource = file_get_contents($dataSourceFile);

        // Skipping parts of file that change at every download altering the check of real differences
        $dataSource = preg_replace('/(<!\-\-\s+.*created=")(.*)("\s+\-\->)/', '$1XXXX-XX-XX XX:XX$3', $dataSource, 1);
        $dataSource = preg_replace('/(<pubDate>)(.*)(<\/pubDate>)/', '$1Xxx, XX Xxx XXXX XX:XX:XX +XXXX$3', $dataSource, 1);


        return hash('md5', $dataSource);
    }

    /**
     * Reads the xml file and return a SimpleXMLElement object
     *
     * If the read fails throws exception
     *
     * @param string $xmlFile XML File to read
     *
     * @return SimpleXMLElement
     * @throws Exception
     */
    private function parseXML($xmlFile)
    {

        libxml_use_internal_errors(true);

        $xml = simplexml_load_file($xmlFile);

        if ($xml === FALSE) {
            throw new Exception($xmlFile . ' is not valid : ' . implode("\n", $this->parseLibXmlErrors()));
        }

        return $xml;
    }

    /**
     * Executes libxml_get_errors() and returns array of messages
     *
     * @param bool $clearErrors If true (default) calls libxml_clear_errors()
     *
     * @return array
     */
    private function parseLibXmlErrors($clearErrors = true)
    {
        $xmlErrors = [];
        foreach (libxml_get_errors() as $xmlErrorObj) {

            $xmlErrorMsg = '';

            switch ($xmlErrorObj->level) {
                case LIBXML_ERR_WARNING :
                    $xmlErrorMsg .= 'LIBXML_ERR_WARNING';
                    break;
                case  LIBXML_ERR_ERROR :
                    $xmlErrorMsg .= 'LIBXML_ERR_ERROR';
                    break;
                case  LIBXML_ERR_FATAL :
                    $xmlErrorMsg .= 'LIBXML_ERR_FATAL';
                    break;
                default :
                    $xmlErrorMsg .= 'LIBXML_ERR_UNKNOWN_LEVEL';
            }

            $xmlErrorMsg .= ' | code=' . $xmlErrorObj->code .
                ' | file=' . $xmlErrorObj->file .
                ' | line=' . $xmlErrorObj->line .
                ' | column=' . $xmlErrorObj->column .
                ' | message=' . $xmlErrorObj->message;

            $xmlErrors[] = $xmlErrorMsg;
        }

        if ($clearErrors == true) {
            libxml_clear_errors();
        }

        return $xmlErrors;
    }

    /**
     * Merges different parts of address into a single string
     *
     * @param string $address
     * @param string $city
     * @param string $country
     * @param string $province
     * @param string $state
     * @param string $zip
     * @param string $state_province
     *
     * @return string
     */
    private function mergeAddress($address, $city, $country, $province, $state, $zip, $state_province)
    {

        // Merge of unique parts
        $prov = [];
        if (trim($province) != '' && strtolower(trim($province)) != strtolower(trim($city))) {
            $prov[strtolower(trim($province))] = trim($province);
        }
        if (trim($state) != '' && strtolower(trim($state)) != strtolower(trim($city))) {
            $prov[strtolower(trim($state))] = trim($state);
        }
        if (trim($state_province) != '' && strtolower(trim($state_province)) != strtolower(trim($city))) {
            $prov[strtolower(trim($state_province))] = trim($state_province);
        }
        $prov_str = trim(implode(" ", $prov));

        // Local parts - all except country
        $local_parts = [];
        if (trim($address) != '') {
            $local_parts[] = trim($address);
        }
        if (trim($zip) != '') {
            $local_parts[] = trim($zip);
        }
        if (trim($city) != '') {
            $local_parts[] = trim($city);
        }
        if (trim($prov_str) != '') {
            $local_parts[] = trim($prov_str);
        }

        // Merged address
        $merged_address = trim(implode(" ", $local_parts));

        // Add country if not empty and if the address is not empty
        if ($merged_address != '' && trim($country) != '') {
            $merged_address .= ', ' . trim($country);
        }

        return $merged_address;
    }

    /**
     * Format event associative to the format used in event comparison
     *
     * @param array $item Associative array that describes an event
     *
     * @return array
     */
    private function eventFormat($item)
    {
        $itemFormatted = [
            'id'             => $item['id'],
            'iCalUID'        => $item['iCalUID'],
            'slug'           => $item['slug'],
            'hash'           => null,
            'status'         => $item['status'],
            'html_link'      => $item['link'],
            'summary'        => (!empty($item['title']) ? $item['title'] : $item['slug']) . ' [' . $item['category_name'] . ']',
            'location'       => $item['_venue_']['full_address'] ?? '',
            'start_ts'       => $item['start_ts'],
            'start_timezone' => $item['timezone'],
            'end_ts'         => $item['end_ts'],
            'end_timezone'   => $item['timezone'],
            'description'    => null,
            'created'        => CommonCalendar::timeToIso8601($item['create_date']),
            'updated'        => null,
        ];

        $description                  = <<<EOD
<b>#title#</b>
<b><i>#sub_title#</i></b>
<u>#category_name#</u>
<a href="#link#"><b>[Scheda evento]</b></a>

<b>Prezzo:</b> #currency# #cost#

<b><u>Informazioni sul luogo:</u></b>
#_venue_.title# #_venue_.full_address# <a href="#_venue_.link#"><b>[Dettagli]</b></a>

<b>Pubblicato:</b> #pub_date#

#excerpt#

#content#

#description#
EOD;
        $itemFormatted['description'] = strtr($description, [
            '#title#'                => $item['title'],
            '#sub_title#'            => $item['sub_title'],
            '#category_name#'        => $item['category_name'],
            '#currency#'             => $item['currency'],
            '#cost#'                 => trim($item['cost']) != '' ? $item['cost'] : '---',
            '#link#'                 => $item['link'],
            '#_venue_.title#'        => $item['_venue_']['title'] ?? '',
            '#_venue_.full_address#' => $item['_venue_']['full_address'] ?? '',
            '#_venue_.link#'         => $item['_venue_']['link'] ?? '',
            '#pub_date#'             => $item['pub_date'],
            '#excerpt#'              => $item['excerpt'],
            '#content#'              => $item['content'],
            '#description#'          => $item['description'],
        ]);

        // Replace image tags (unsupported in Google Calendar event description)
        $itemFormatted['description'] =
            preg_replace('/<img\s+src="([^"]+)"[^>]+>/i',
                '<div class="wp-image-replacement">' .
                '<a href="$1"><b>[IMAGE]</b></a>' .
                '</div>',
                $itemFormatted['description']);

        $itemFormatted['hash'] = CommonCalendar::calcEventHash($itemFormatted);

        return $itemFormatted;
    }
}